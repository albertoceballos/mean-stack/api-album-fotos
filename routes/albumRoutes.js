'use strict'

var express=require('express');
var api=express.Router();

var albumController=require('../controllers/albumController');

api.get('/albums',albumController.getAlbums);
api.get('/album/:id',albumController.getAlbum);
api.post('/album',albumController.saveAlbum);
api.put('/album/:id',albumController.upadateAlbum);
api.delete('/album/:id',albumController.deleteAlbum);
module.exports=api;